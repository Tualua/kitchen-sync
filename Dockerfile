ARG APP_DIR="/opt"
FROM ubuntu:22.04 as build
ARG VERSION
ARG APP_DIR
RUN apt update \
    && apt install git build-essential cmake libpq-dev libmysqlclient-dev -y
WORKDIR $APP_DIR
RUN git clone --depth 1 --branch ${VERSION} https://github.com/willbryant/kitchen_sync.git
RUN cd kitchen_sync/build && cmake .. && make

FROM ubuntu:22.04 as prod
ARG APP_DIR
RUN apt update \
    && apt install libpq5 libmysqlclient21 -y \
    && apt clean

WORKDIR $APP_DIR
COPY --from=build $APP_DIR/kitchen_sync/build/ks* /usr/bin/

ARG APP_DIR="/opt"
FROM ubuntu:22.04 as build
ARG VERSION
ARG APP_DIR
RUN apt update \
    && apt install git build-essential cmake libpq-dev libmysqlclient-dev -y
WORKDIR $APP_DIR
RUN git clone --depth 1 --branch ${VERSION} https://github.com/willbryant/kitchen_sync.git
RUN cd kitchen_sync/build && cmake .. && make

FROM ubuntu:22.04 as prod
ARG APP_DIR
RUN apt update \
    && apt install libpq5 libmysqlclient21 -y \
    && apt clean

WORKDIR $APP_DIR
COPY --from=build $APP_DIR/kitchen_sync/build/ks* /usr/bin/

CMD ["ks", "--from", "mysql://$MYSQL_USER:$MYSQL_PASSWORD@mysql/$MYSQL_DATABASE", "--to", "postgresql://$DB_USERNAME:$DB_PASSWORD@$DB_HOST:$DB_PORT/$DB_DATABASE"]

